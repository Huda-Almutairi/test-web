FROM python:3.9

COPY . /tmp/
WORKDIR /tmp

RUN pip install -r requirements.txt
ENV FLASK_APP main

CMD ["flask", "run", "--host=0.0.0.0"]

